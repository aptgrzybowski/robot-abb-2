from time import sleep
# do pick red block three and drop over red mark or grab green block and drop over green mark

class Walker:
    def __init__(self, store):
        self.functions = dict()
        self.store = store
        self.visited = []

    def and_op(self, node):
        if node.token_index in self.visited:
            return {'type': 'ERROR'}    

        action = self.walk(node.children[0])
        if action['type'] == 'OBJ_ERROR':
            self.visited.append(node.token_index)
            return action

        if action['type'] == 'ERROR':
            action = self.walk(node.children[1])
            return action

        return action

    def then_op(self, node):
        if node.token_index in self.visited:
            return {'type': 'ERROR'}    

        action = self.walk(node.children[0])
        if action['type'] == 'ERROR':
            action = self.walk(node.children[1])
            return action
        return action

    def or_op(self, node):
        if node.token_index in self.visited:
            return {'type': 'ERROR'}    
        
        action = self.walk(node.children[0])
        if action['type'] == 'ERROR' or action['type'] == 'OBJ_ERROR':
            self.visited.append(node.children[0].token_index)
            action = self.walk(node.children[1])
            return action
        else:
            self.visited.append(node.children[1].token_index)
            return action

    # def then(self, node):
    def verb(self, node):
        if node.token_index in self.visited:
            return {'type': 'ERROR'}    

        obj = self.compile_object(node.children[0])
        # print('KURAWAAAAAAAAAAAAAAAAAAAAAA', obj[0])
        self.visited.append(node.token_index)
        if len(obj) == 0:
            return {'type': 'OBJ_ERROR'}    
        return {'type': node.token[1], 'coords': obj[0]['coords']}

    def put_tree(self, ast):
        self.ast = ast
        self.visited = []

    def compile_object(self, object_node):
        infos = {'color': None, 'noun': None, 'number': 'zero'}
            
        children_info = {child.token[0]: child.token[1] for child in object_node.children}
        infos.update(children_info)
        infos.update({object_node.info[0]: object_node.info[1]})
        fast_map = {'zero': None, 'one': 1, 'two': 2, 'three': 3, 'four': 4}
        id = (infos['color'], infos['noun'], fast_map[infos['number']])
        print('id',id)
        objects = self.store.find_objects(id)
        print(objects)
        return objects
    
    def walk(self, node):
        if node.token[0] == 'verb':
            return self.verb(node)
        elif node.token[1] == 'or':
            return self.or_op(node)
        elif node.token[1] == 'then':
            return self.then_op(node)
        elif node.token[1] == 'and':
            return self.and_op(node)
        
    def get_action(self):
        action = self.walk(self.ast)
        if action == None:
            action = {'type': 'CODE_ERROR'}
        # TODO wywal sleep to tlyko dyo ssymulacji
        sleep(1)
        # if self.loop and action['type'] == 'ERROR':
            # print('< next loop >')
            # sleep(1)
            # self.visited = []
            # action = {'type': 'LOOP'}

        return action
        
