from time import sleep
class Commander:
    # mozliwe ze dispatcher nie bedzie potrzebny
    def __init__(self, store, dispatcher, parser, walker):
        self.store = store
        self.dispatcher = dispatcher
        self.parser = parser
        self.walker = walker
        self.emergency_stop = False

    def program(self, function_name, function_body):
        ast = self.parser.create_ast(function_body)
        self.store.put_program(function_name, ast, function_body)
        print("dodano program:", function_name)

    def run(self, function_name, target):
        ast = self.store.get_program(function_name)
        loop = False
        if ast.token[1] == 'repeat':
            loop = True
            ast = ast.children[0]
        while True:
            self.walker.put_tree(ast)
            while True:
                action = self.walker.get_action()
                if self.emergency_stop:
                    # action = {'type': 'stop'}
                    # self.dispatcher.put_action_target(action ,target)
                    break
                if action['type'] == 'CODE_ERROR':
                    print('CODE ERROR!')
                    break
                if action['type'] == 'ERROR' or action['type'] == 'OBJ_ERROR':
                    break
                self.dispatcher.put_action_target(action ,target)
                self.dispatcher.wait()
            if self.emergency_stop:
                self.emergency_stop = False 
                break
            if not loop:
                break

        print('koniec programu: ', function_name)

    def do(self, function_body, target):
        self.program('TMP', function_body)
        self.run('TMP', target);

    def stop(self):
        print('STOP')
        self.emergency_stop = True
        
    def reset(self):
        print('RESET')
        self.dispatcher.put_action_target({'type': 'reset'} ,'global')
        
                
    def handle_command(self, command):
        type = command['type']
        if 'args' in command:
            args = command['args']

        if type == 'program':
            self.program(*args)

        if type == 'run':
            self.run(*args)

        if type == 'do':
            self.do(*args)

        if type == 'error':
            return

        if type == 'stop':
            self.stop()

        if type == 'reset':
            self.reset()

