# teoretycznie caly gotowy
# TODO cos sie psuje jesli dodaje sie dwie komenddy jednoeczesnie
from queue import Queue
from time import sleep
import json

# empty class
class Empty: pass

class Dispatcher:
    def __init__(self):
        self.subscribers = {}
        self.action_target_queue = Queue()

    def check_ready(self):
        print('sprawdzone ready')
        for key, subscriber in self.subscribers.items():
            if subscriber.is_ready == False:
                return False 
        return True

    def unblock_subscribers(self, action, target):
        if target == 'global':
            for key, subscriber in self.subscribers.items():
                subscriber.block_queue.put(action)
        else:
            self.subscribers[target].block_queue.put(action)

    def put_action_target(self, new_action, new_target):
        self.action_target_queue.put((new_action, new_target))
        print('dodaje akcje do action_target_queue', new_action, 'wielkosc kolejki', self.action_target_queue.qsize())
        if self.check_ready():
            action, target = self.action_target_queue.get()
            self.unblock_subscribers(action, target)

    # ("robot")
    def add_subscriber(self, subscriber_id):
        subscriber = Empty()
        subscriber.is_ready = True
        subscriber.block_queue = Queue()
        self.subscribers[subscriber_id] = subscriber; 
        # sprawdzanie czy wszyscy sie zwolnili i czy jest cos w kolejce
        if self.action_target_queue.qsize() > 0 and self.check_ready():
            action, target = self.action_target_queue.get()
            self.unblock_subscribers(action, target)

        action = subscriber.block_queue.get(block=True)
        subscriber.is_ready = False

        print("wyslano akcje do", subscriber_id)
        return json.dumps(action)

    def wait(self):
        while not self.check_ready():
            sleep(0.1)
        print('poczekane')

