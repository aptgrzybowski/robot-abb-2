# GRAB ZAMIENIONY NA PICK
token_counter = 0
class Parser:
    def __init__(self):
        # typy w odpowiedniej kolejnosci dzialania
        self.types = {
            "mode": ["repeat"],
            "operator": ["then", "or", "and"],
            "verb": ["grab", "move over", "drop over"],
            "noun": ["block", "mark"],
            "color": ["red", "green", "blue", "yellow"],
            "number": ["one", "two", "three", "four", "five"],
        }
        self.op_order = self.flatten_types()

    # string do sparsowania
    def lexer(self, rest):
        rest = rest.replace('pick up', 'grab')
        token_list = []
        flat_types = []
        for cat, words in self.types.items():
            for w in words:
                flat_types.append((cat, w))
            
        while len(rest) > 0:
            break_flag = False
            for el in flat_types:
                cat, w = el
                if rest.startswith(w):
                    # print('w:',w,'rest:',rest)
                    token_list.append((cat, w))
                    rest = rest[len(w):].strip()
                    break_flag = True
                    break
            if not break_flag:
                try: 
                    index = rest.index(' ')
                    err = rest[0:index]
                    rest = rest[rest.index(' '):].strip()
                    print('blad w parsowaniu, nie ma slowa:',err)
                except:
                    print('blad w parsowaniu, nie ma slowa:',rest)
                rest = ''
        print('token list', token_list)
        return token_list

    # splaszcza slownik typow do 1 wymiarowej tablicy
    def flatten_types(self):
        arr = []
        for cat, words in self.types.items():
            for w in words:
                arr.append(w)
        return arr

    def create_ast(self, command_text):
        global token_counter
        token_counter = 0
        lexed = self.lexer(command_text)
        root = self.Node(lexed, 0, parser=self)
        return root


    # wezel drzewa 
    class Node:
        def __init__(self, desc_tokens, current_op, parent=None, parser=None):
            global token_counter
            self.desc_tokens = desc_tokens
            self.parent = parent
            self.parser = parser
            # print(parent, "bleee")
            self.children = []
            self.current_op = current_op
            self.token_index = token_counter 
            token_counter += 1
            while current_op < len(parser.op_order): 
                try:
                    found_index = [y for x, y in desc_tokens].index(parser.op_order[current_op])
                    self.token = desc_tokens[found_index]
                    print(self.token, self.token_index, " <= ", self.parent.token if self.parent else "brak")
                    # print(desc_tokens[found_index], self.token_index, " <= ", self.parent.token if self.parent else "brak")
                    arr1 = desc_tokens[:found_index]
                    if arr1:
                        self.children.append(parser.Node(arr1, current_op, self, parser))

                    arr2 = desc_tokens[found_index + 1:]
                    if arr2:
                        self.children.append(parser.Node(arr2, current_op, self, parser))
                    break
                except ValueError:
                    current_op += 1
            self.info = desc_tokens[found_index]

        # def parser(token_list):

# parser = Parser()
# print('konstrukcja')
# ast = parser.create_ast('grab red block')
