from threading import Lock

class Store:
    def __init__(self):
        self.programs = {}
        self.raw_programs = {}
        self.objects = []
        self.programs_lock = Lock() 
        self.raw_programs_lock = Lock() 
        self.objects_lock = Lock() 
       

    def put_program(self, function_name, ast, function_body):
        with self.programs_lock:
            self.programs[function_name] = ast

        with self.raw_programs_lock:
            print('\x1b[31m', function_body, '\x1b[m')
            self.raw_programs[function_name] = function_body

    def put_objects(self, new_objects):
        with self.objects_lock:
            self.objects = new_objects
            # print(self.objects)

    def get_objects(self):
        with self.objects_lock:
            return self.objects

    def get_program(self, program_name):
        with self.programs_lock:
            if  program_name in self.programs:
                return self.programs[program_name]
            else:
                return "error: brak programu"

    def get_raw_programs(self):
        with self.raw_programs_lock:
            return self.raw_programs

    # id w tupli ('red', 'block', 1) itp. 
    def find_objects(self, id):
        objects = self.get_objects()
        # print('all', objects)
        for i in range(3):
            if id[i] != None:
                objects = list(filter(lambda obj: obj['id'][i] == id[i], objects)) 
        return objects
