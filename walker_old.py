# TODO do zmiany do myslenia
class Walker:
    def __init__(self, ast, store):
        self.ast = ast
        self.store = store

    def compile_object(self, object_node):
        # children_info = [child.info for child in object_node.children]
        infos = {'color': None, 'noun': None, 'number': 'one'}
        children_info = {child.info[0]: child.info[1] for child in object_node.children}
        infos.update(children_info)
        infos.update({object_node.info[0]: object_node.info[1]})
        fast_map = {'one': 1, 'two': 2, 'three': 3, 'four': 4}
        id = (infos['color'], infos['noun'], fast_map[infos['number']])
        # print(id)
        objects = self.store.get_objects()
        # print(objects)
        obj = None
        for object in objects:
            id_old = tuple(object['id'])
            # print(id_old, ' == ', id)
            if id_old == id:
                obj = object
        return obj['coords']
    
        
    def get_action(self):
        coords = self.compile_object(self.ast.children[0])
        action = {'type': self.ast.info[1], 'coords': coords}
        # print(action)
        return action
        
