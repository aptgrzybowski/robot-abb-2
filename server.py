# TODO /simulation /vision /command /acton chyba ok. dodac reszty trybow 
from flask import Flask, render_template, request
from flask_cors import CORS
import json

from commander import Commander
from store import Store
from dispatcher import Dispatcher
from parser import Parser
from walker import Walker


store = Store()
dispatcher = Dispatcher() 
parser = Parser()
walker = Walker(store)

commander = Commander(
    store=store,
    dispatcher=dispatcher,
    walker=walker,
    parser=parser)

app = Flask(__name__)
CORS(app)

@app.route('/simulation')
def simulation():
    return render_template('index.html') 

#TODO tu daj storne matiego
@app.route('/control_panel')
def control_panel():
    return render_template('index.html') 

@app.route('/get_programs')
def get_programs():
    return json.dumps(store.get_raw_programs())

@app.route('/get_objects')
def get_objects():
    return json.dumps(store.get_objects())

# {'id': ('red', 'block', 1), 'coords': [100,100,0.5]}
@app.route('/vision', methods=['POST'])
def vision_interface():
    data = request.get_json()
    store.put_objects(data)
    return "vision interface" 

@app.route('/command', methods=['POST'])
def command_interface():
    command = request.get_json()
    commander.handle_command(command)
    return json.dumps(command)

# tablica z id np [robot]
@app.route('/action', methods=['POST'])
def action_interface():
    return dispatcher.add_subscriber(request.get_json()[0]) 
    
if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
