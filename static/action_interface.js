// mega chwilowe zwlaszcza toz tmp
// TODO zepnij to z virtualnym robotem
class Action_interface {
    constructor(server_address, name) {
	this.server_address = server_address; 
	this.name = name;
    }

    async send_action_req() {
	const response = await fetch(`http://${this.server_address}/action`, {
	    method: "POST",
	    headers: {
		'Content-Type': 'application/json',
	    },
	    body: JSON.stringify([this.name]),
	});
	let tmp = await response.json();
	console.log(tmp)
	return tmp 
    }

    // blokuje skrypt na long polling  
    async connect() {
	while (true) {
	    // console.log('wysylame nowy action req')
	    await this.send_action_req();	
	    // console.log('dostalem nowy action req')
	}
    }
}
