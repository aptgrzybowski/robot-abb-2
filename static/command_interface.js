// teoretycznie wszystko gotowe nie liczac do. Przemysl jak to ma sie zachowywac
class Command_interface {
    constructor(server_address) {
	this.server_address = server_address; 
	console.log(this.server_address);
    }
    
    parse_to_command(text) {
	const words = text.split(' ');

	if (words[0] === 'stop')
	    return {type: 'stop', args: [], text};

	if (words[0] === 'reset')
	    return {type: 'reset', args: [], text};

	if (words.includes('is')) {
	    const split_index = words.indexOf('is');
	    // console.log(split_index);
	    const function_name = words.slice(0, split_index).join(' ');
	    const function_body = words.slice(split_index + 1).join(' ');
	    return {type: 'program', args: [function_name, function_body], text};
	}

	if (words[0] == 'run') {
	    const split_index = words.indexOf('on');
	    let function_data;
	    if  (split_index == -1) 
		function_data = words.slice(1).join(' ');
	    else
		function_data = words.slice(1, split_index).join(' ');
	    // console.log(words);
	    const target = (split_index != -1) ? words.slice(split_index + 1).join(' ') : 'global';
	    return {type: words[0], args: [function_data, target], text};
	}

	// zmodyfikuj dzialanie zeby obciac do
	if (words[0] == 'please') {
	    const split_index = words.indexOf('on');
	    const function_data = words.slice(1).join(' ');
	    const target = (split_index != -1) ? words.slice(split_index + 1).join(' ') : 'global';
	    return {type: 'do', args: [function_data, target], text};
	}

	return {type: "error"}
    }
    async send_command(text) {
	const command = this.parse_to_command(text);
	
	// pamietaj zeby byl command i ma sie zaczynac od http://
	const response = await fetch(`http://${this.server_address}/command`, {
	    method: "POST",
	    // Access-Control-Allow-Origin: 'https://javascript.info',
	    headers: {
		'Content-Type': 'application/json',
		// 'Access-Control-Allow-Origin': `http://${this.server_address}`,
	    },
	    body: JSON.stringify(command),
	});
	let data = await response.json();
	// console.log(data);
    }
}
