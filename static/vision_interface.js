class Vision_interface {
    constructor(server_address) {
	this.server_address = server_address; 
	this.store = [
	    {id: ['red', 'block', 1], coords: [200, 200, 0.5]},
	    {id: ['red', 'block', 2], coords: [-100, 100, 0.5]},
	    {id: ['green', 'block', 1], coords: [-100, 100, 0.5]},
	];
    }

    async send_objects() {
	const response = await fetch(`http://${this.server_address}/vision`, {
	    method: "POST",
	    headers: {
		'Content-Type': 'application/json',
	    },
	    body: JSON.stringify(this.store),
	});
    }
}
